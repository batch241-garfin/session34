
const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [
	{
		"username": "romhel",
		"password": "rom123"
	}
	];

// 1-2.

app.get("/home", (request, response) => {

	response.send("Welcome to the home page")
})

// 3-4.

app.get("/users", (request, response) => {

	response.send(users)
})

// 5-6.

app.delete("/delete-user", (request, response) => {

	response.send(`${request.body.username} has been deleted`)
})








app.listen(port, () => console.log(`Server is running at port: ${port}`));